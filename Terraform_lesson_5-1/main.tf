provider "google" {
  credentials = file("/root/.ssh/gcloud/gcloud_cred.json")

  project = "x-router-347107"
  region  = "us-central1"
  zone    = "us-central1-c"
}

variable "node_amount" {
  type    = number
  default = 3
}

variable "priv_key_path" {
  default = "/root/.ssh/gcloud/id_rsa"
}

resource "google_compute_instance" "default" {
  count        = var.node_amount
  name         = "ter-less-1-number-${count.index + 1}"
  machine_type = "e2-small"
  tags = ["web"]
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      size  = "20"
    }
  }

  provisioner "local-exec" {
    command = "echo hostname: ${self.name} IP: ${self.network_interface.0.access_config.0.nat_ip} >> host.list"
  }

  connection {
    user = "ketroon"
    private_key = file(var.priv_key_path)
    host = self.network_interface.0.access_config.0.nat_ip
  }

  provisioner "remote-exec" {
    inline = [ "sudo apt install -y nginx", "sudo touch /var/www/html/index.html && sudo chmod 666 /var/www/html/index.html","sudo echo Juneway $HOSTNAME $(curl -H 'Metadata-Flavor: Google' http://169.254.169.254/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip)  > /var/www/html/index.html"]
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}

resource "google_compute_firewall" "default" {
 name    = "web-firewall"
 network = "default"

 allow {
   protocol = "tcp"
   ports    = ["80"]
 }

 source_ranges = ["0.0.0.0/0"]
 target_tags = ["web"]
}
