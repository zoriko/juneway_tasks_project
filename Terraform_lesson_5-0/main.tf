provider "google" {
  credentials = file("x-router-347107-f0e1dded45de.json")

  project = "x-router-347107"
  region  = "us-central1"
  zone    = "us-central1-c"
}

variable "node_amount" {
  type    = number
  default = 3
}

resource "google_compute_network" "vpc-network" {
  name = "terraform-network"
}

resource "google_compute_instance" "default" {
  count        = var.node_amount
  name         = "ter-less-1-number-${count.index + 1}"
  machine_type = "e2-small"
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      size  = "20"
    }
  }
  network_interface {
    network = "terraform-network"
  }
}
